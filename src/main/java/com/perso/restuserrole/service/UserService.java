package com.perso.restuserrole.service;

import com.perso.restuserrole.model.User;

import java.util.Collection;
import java.util.Optional;

public interface UserService {

    Collection<User> getAllUsers();

    Optional<User> findUserById(Long id);

    Optional<User> findByLogin(String login);

    User saveOrUpdateUser(User user);

    void deleteUser(Long id);

}
