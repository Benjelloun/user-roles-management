package com.perso.restuserrole.service;


import com.perso.restuserrole.dao.IRoleRepository;
import com.perso.restuserrole.model.Role;
import org.apache.commons.collections4.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Stream;

@Service(value = "roleService")
public class RoleServiceImpl implements RoleService {


    private IRoleRepository roleRepository;

    public RoleServiceImpl() {
        super();
    }
    @Autowired //autowired par constructeur pour bénéficier des tests unitaires
    public RoleServiceImpl(IRoleRepository roleRepository) {
        super();
        this.roleRepository = roleRepository;
    }


    @Override
    public Collection<Role> getAllRoles() { //Avant JAVA8

        return IteratorUtils.toList(roleRepository.findAll().iterator());
    }

    @Override
    public Stream<Role> getAllRolesStream() {//JAVA8

        return roleRepository.getAllRolesStream();
    }

    @Override
    public Role findByRoleName(String roleName) {

        return roleRepository.findByRoleName(roleName);
    }
}
