package com.perso.restuserrole.controller;

import com.perso.restuserrole.dao.IUserRepository;
import com.perso.restuserrole.exception.BusinessResourceException;
import com.perso.restuserrole.model.User;
import com.perso.restuserrole.service.RoleService;
import com.perso.restuserrole.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.Collection;
import java.util.Optional;

@Controller
@CrossOrigin(origins = "http://localhost:8080", maxAge = 3600)
@RestController
@RequestMapping("/user")
@Api(value="Web services ", description="Operations for USER ")

public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;
    @Autowired
    IUserRepository iUserRepository;

    @GetMapping(value = "/users" , produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Recover all of users")
    public ResponseEntity<Collection<User>> getAllUsers() {
        Collection<User> users = userService.getAllUsers();
        logger.info("liste des utilisateurs : " + users.toString());
        return new ResponseEntity<Collection<User>>(users, HttpStatus.FOUND);
    }

    @PostMapping(value = "/create" , produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Add user")
    public ResponseEntity<User> saveUser(@RequestBody User user) {
        User userSaved = iUserRepository.save(user);
        return new ResponseEntity<User>(userSaved, HttpStatus.CREATED);
    }
    @DeleteMapping(value = "/users" , produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "delete user")
    public ResponseEntity<Void> deleteUser(@RequestParam(value = "id", required = true) Long id) throws BusinessResourceException {
        userService.deleteUser(id);
        return new ResponseEntity<Void>(HttpStatus.GONE);
    }


    @GetMapping(value = "/users/{id}" , produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "recover one user by his ID")
    public ResponseEntity<User> findUserById(@PathVariable(value = "id") Long id ) {
        Optional<User> userFound = userService.findUserById(id);
        return new ResponseEntity<User>(userFound.get(), HttpStatus.FOUND);
    }

    @GetMapping(value = "/users/" , produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Recover one user by his LOGIN")
            public ResponseEntity<User> findByLogin(@RequestParam(value = "login") String login ) {
        Optional<User> userFound = userService.findByLogin(login);
        return new ResponseEntity<User>(userFound.get(), HttpStatus.FOUND);
    }

    @PutMapping(value = "/update/{id}" , produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Update user by ID ")
            public User updateUser(@PathVariable(value = "id") Long id, @RequestBody User user){
        if(id !=null){
            Optional<User> us = iUserRepository.findById(id);
            if(us != null){
                user.setId(id);
                return iUserRepository.save(user);
            }
        }
        return null;
    }
}
