package com.perso.restuserrole.dao;

import com.perso.restuserrole.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface IUserRepository extends JpaRepository<User, Long> {
    Optional<User> findByLogin(String login);

}
